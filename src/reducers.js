import { combineReducers } from 'redux';
import merchantListReducer from 'containers/MerchantList/reducer';

export default combineReducers({
  merchantList: merchantListReducer,
});
