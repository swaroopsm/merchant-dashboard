import {
  get,
  post,
  put,
  destroy,
} from  '../utils/api';
import { name, internet, random, phone, commerce, date, lorem } from 'faker';
import range from 'lodash/range';
import shuffle from 'lodash/shuffle';
import randomNumberInRange from 'lodash/random';

const CAR_MANUFACTURERS = [
  'Toyota',
  'Hyundai',
  'Chevrolet',
  'BMW',
  'Volkswagen',
  'Ferrari',
  'Skoda',
];

const getCarBrand = () => (
  [shuffle(CAR_MANUFACTURERS)[0], lorem.word()].join(' ')
)

const getBidListSeed = (count) => (
  range(0, count)
    .map(getBidSeed)
);

const getBidSeed = () => ({
  id: random.number(),
  carTitle: getCarBrand(),
  amount: parseInt(commerce.price(), 10),
  created: date.past() ,
});

const getMerchantSeed = ({ bidCount }) => ({
  id: random.number(),
  firstname: name.firstName(),
  lastname: name.lastName(),
  avatarUrl: internet.avatar(),
  email: internet.email(),
  phone: phone.phoneNumberFormat(),
  hasPremium: shuffle([true, false])[0],
  bids: getBidListSeed(bidCount),
});

const getMerchantListSeed = ({ merchantsCount, maxBids }) => (
  range(0, merchantsCount)
    .map((i) => getMerchantSeed({ bidCount: maxBids }))
);

const TOTAL_MERCHANTS = randomNumberInRange(90,180);

export function getMerchantList({ from, to }) {
  return get(`/merchants?from=${from}&to=${to}`)
    .then(() => ({
      total: TOTAL_MERCHANTS,
      merchants: getMerchantListSeed({ merchantsCount: 18, maxBids: 9 })
    })
  )
}

export function createMerchant({ firstname, lastname, email, phone, avatarUrl }) {
  return post('/merchants')
    .then(() => ({
      id: `${random.number()}-new`,
      firstname,
      lastname,
      avatarUrl: avatarUrl,
      email,
      phone,
      hasPremium: shuffle([true, false])[0],
      bids: [],
    }));
}

export function updateMerchant({ id, firstname, lastname, email, phone, avatarUrl, hasPremium }) {
  return put(`/merchants/${id}`)
    .then(() => ({
      id,
      firstname,
      lastname,
      email,
      phone,
      avatarUrl,
      hasPremium,
    }));
}

export function deleteMerchant(merchantId) {
  return destroy(`/merchants/${merchantId}`);
}
