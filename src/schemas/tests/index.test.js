import {
  Merchant,
  MerchantList,
  Bid,
  merchantListNormalizer,
} from '../index';

it('contains Merchant schema', () => {
  expect(Merchant).toBeDefined();
});

it('contains Bid schema', () => {
  expect(Bid).toBeDefined();
});

it('merchant has a list of bids associated to it', () => {
  expect(Merchant.schema.bids).toBeDefined();
});

it('contains MerchantList schema', () => {
  expect(MerchantList).toBeDefined();
});

it('normalizes merchant list correctly', () => {
  const result = merchantListNormalizer([{
    id: 1,
    name: 'John Doe',
    bids: [{id: 2, carTitle: 'Toyota Prius'}],
  }]);

  expect(result.entities.merchants).toBeDefined();
  expect(result.entities.bids).toBeDefined();
  expect(result.result.length).toEqual(1);
});
