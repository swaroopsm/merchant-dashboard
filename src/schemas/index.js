import { schema, normalize } from 'normalizr';

// Bid Schema
export const Bid = new schema.Entity('bids');

// Merchant Schema
export const Merchant = new schema.Entity('merchants', {
  bids: [Bid],
});

// Merchant Collection / List
export const MerchantList = new schema.Array(Merchant);

export const merchantListNormalizer = (data) => normalize(data, MerchantList);
export const merchantItemNormalizer = (data) => normalize(data, Merchant);
