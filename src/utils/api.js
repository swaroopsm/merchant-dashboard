export function createHTTPMethod(type) {
  return function httpMethod() {
    // Mocking the API, an making it promise based.
    // Going forward this can easily be replaced by someting like fetch / superagent
    return new Promise((resolve, reject) => {
      // Currently all calls to this promise will be successful
      resolve({});
    });
  }
}

export const get = createHTTPMethod('GET');
export const post = createHTTPMethod('POST');
export const put = createHTTPMethod('PUT');
export const destroy = createHTTPMethod('DELETE');
