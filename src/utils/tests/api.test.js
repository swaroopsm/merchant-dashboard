import {
  get,
  post,
  destroy,
  put,
  createHTTPMethod,
} from '../api';

describe('createHTTPMethod', () => {
  let httpMethod;

  beforeEach(() => {
    httpMethod = createHTTPMethod('GET');
  });

  it('creates a HTTP based method correctly', () => {
    expect(typeof httpMethod).toEqual('function');
  });

  it('created http method when executed returns a promise', () => {
    const result = httpMethod();

    expect(result.then).toBeDefined();
    expect(result.catch).toBeDefined();
  });
})

describe('get', () => {
  it('has get to make a GET request', () => {
    expect(get).toBeDefined();
  });

  it('returns a promise when executed', () => {
    // In a real world scenario we should try to mock this
    const result = get('http://hello.world');

    expect(result).toEqual(
      expect.objectContaining({
        then: expect.any(Function),
        catch: expect.any(Function),
      })
    );
  });
});

describe('post', () => {
  it('has post to make a POST request', () => {
    expect(post).toBeDefined();
  });
});

describe('put', () => {
  it('has put to make a PUT request', () => {
    expect(put).toBeDefined();
  });
});

describe('destroy', () => {
  it('has destroy to make a DELETE request', () => {
    expect(destroy).toBeDefined();
  });
})
