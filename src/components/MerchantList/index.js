import React from 'react';
import MerchantItem from 'components/MerchantItem';
import { Col, Row } from 'react-flexbox-grid';
import styled from 'styled-components';

const Wrapper = styled(Row)`
`;

const MerchantItemWrapper = styled(Col)`
  margin-bottom: 12px;
  width: 100%;
`;

function MerchantList(props) {
  return (
    <Wrapper>
    {
      props.merchants.map((merchant) => (
        <MerchantItemWrapper xs={12} md={6} lg={6} key={merchant.id}>
          <MerchantItem
            merchant={merchant}
            onEdit={props.onEdit}
            onRemove={props.onRemove}
            onShowMoreBids={props.onOpenBids}
          />
        </MerchantItemWrapper>
      ))
    }
    </Wrapper>
  );
}

export default MerchantList;
