import React from 'react';
import BidItem from 'components/BidItem';
import styled from 'styled-components';
import { Row, Col } from 'react-flexbox-grid';
import Modal from 'components/Modal';

const Wrapper = styled.div`
  margin: 0 auto;
`;

const ItemWrapper = styled(Row)`
  margin-bottom: 12px;
`;

function BidsModal({ bids, ...rest }) {
  return (
    <Modal {...rest} title="Bids History">
      <Wrapper>
        {
          bids.map((bid) => (
            <ItemWrapper key={bid.id}>
              <Col xs={12}>
                <BidItem bid={bid} />
              </Col>
            </ItemWrapper>
          ))
        }
      </Wrapper>
    </Modal>
  );
}

export default BidsModal;
