import React from 'react';
import styled, { css } from 'styled-components';
import {
  $inputBorderColor,
  $inputBg,
  $inputSelectedBorderColor,
  $inputErrorBorderColor,
  $inputLabelColor,
  $inputBoxShadowColor,
  $inputBoxHeight,
  $inputHighlightSelectedBorderColor,
} from 'styles/variables';

const getWidth = ({ inline }) => (
  inline ? 'initial' : '100%'
);

const getInputBorderColor = ({ error }) => (
  error ? $inputErrorBorderColor : $inputBorderColor
);

const getSelectedBorderColor = ({ error, highlight, selected }) => {
  if (selected) {
    return highlight ? $inputHighlightSelectedBorderColor : $inputSelectedBorderColor;
  }

  return error ? $inputErrorBorderColor : $inputBorderColor;
};

const getLabelColor = ({ error, selected, highlight }) => {
  if (selected) {
    return highlight ? $inputHighlightSelectedBorderColor : $inputLabelColor;
  }

  if (error) {
    return $inputErrorBorderColor;
  }

  return $inputLabelColor;
};

const getBoxShadow = ({ error, selected, flat }) => {
  if (flat) { return null; }

  return error && !selected ? null : `inset 2px 2px 10px ${$inputBoxShadowColor}`;
};

const getInputBg = ({ highlight, opaque }) => {
  if (highlight) { return $inputBg; }
  if (opaque) { return 'white'; }

  return 'white';
};

const getCommonInputProps = () => css`
  border: 1px solid ${getInputBorderColor};
  padding: ${(props) => (props.condensed ? '5px; 10px' : '18px 10px 10px 10px')};
  background-color: ${getInputBg};
  color: black;
  width: ${getWidth};
  box-shadow: none;
`;

const getFocusedStyles = () => css`
  outline: 0;
  border: 2px solid ${getSelectedBorderColor};
  box-shadow: ${getBoxShadow};
  background: white;
`;

export const InputLabel = styled.label`
  position: absolute;
  color: rgba(0, 0, 0, .6);
  pointer-events: none;
  will-change: transform;
  transition: all 0.2s;
  top: ${({ condensed }) => (condensed ? '6px' : '12px')};
  left: 0px;
  padding-left: 10px;
  color: ${getLabelColor};
  transform-origin: left;

  ${({ floating }) => {
    if (floating) {
      return css`
        left: 5px;
        font-weight: ${({ theme }) => (theme.mediumWeight)};
        transform: translateY(-50%) scale(.70, .70)
      `;
    }

    return null;
  }};
`;

export const InputBox = styled.input`
  ${getCommonInputProps};

  &:focus {
    ${getFocusedStyles};
  }
`;

export const MultiInputBox = styled.textarea`
  resize: none;
  min-height: 96px;
  ${getCommonInputProps};

  &:focus {
    ${getFocusedStyles};
  }
`;

const InputFieldContainer = styled.div`
  position: relative;
  height: ${$inputBoxHeight};
  box-sizing: border-box;

  > ${InputBox}, > ${MultiInputBox} {
    border-radius: 2px;
    background: ${getInputBg};
    transition: box-shadow 0.2s;
    width: 100%;
    box-sizing: border-box;
    font-weight: ${({ theme }) => (theme.normalWeight)};
  }
`;

class Input extends React.PureComponent {
  // static propTypes = {
  //   value: PropTypes.oneOfType([
  //     React.PropTypes.string,
  //     React.PropTypes.number,
  //   ]),
  //   label: PropTypes.string,
  //   error: PropTypes.boolean,
  //   className: PropTypes.string,
  //   name: PropTypes.string,
  //   type: PropTypes.string,
  //   floatingLabel: PropTypes.bool,
  //   condensed: PropTypes.bool,
  //   highlight: PropTypes.bool,
  //   opaque: PropTypes.bool,
  //   required: PropTypes.any,
  //   flat: PropTypes.bool,
  //   multiline: PropTypes.bool,
  //   autoFocus: PropTypes.bool,
  //   inline: PropTypes.bool,
  //   onChange: PropTypes.func,
  //   onFocus: PropTypes.func,
  //   onBlur: PropTypes.func,
  // };

  static defaultProps = {
    error: '',
    floatingLabel: true,
    condensed: false,
    highlight: true,
    type: 'text',
    multiline: false,
    autoFocus: false,
    inline: false,
    opaque: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      focused: false,
    };
  }

  onFocus = (...args) => {
    this.setState({
      focused: true,
    });

    this.props.onFocus && this.props.onFocus(...args); // eslint-disable-line no-unused-expressions
  }

  onBlur = (...args) => {
    this.setState({
      focused: false,
    });

    this.props.onBlur && this.props.onBlur(...args); // eslint-disable-line no-unused-expressions
  }

  getLabel() {
    if (this.state.focused) {
      return this.props.label;
    }

    return this.props.error || this.props.label;
  }

  getValidators() {
    let validators = {};

    if (this.props.required) {
      validators = { ...validators, required: true };
    }

    return validators;
  }

  isLabelFloating() {
    return this.props.error || !!this.props.value || this.state.focused;
  }

  render() {
    const InputComponent = this.props.multiline ? MultiInputBox : InputBox;

    return (
      <InputFieldContainer
        opaque={this.props.opaque}
        className={this.props.className}
      >
        <InputComponent
          type={this.props.type}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          value={this.props.value}
          onChange={this.props.onChange}
          placeholder={this.props.floatingLabel ? '' : this.props.label}
          condensed={this.props.condensed}
          highlight={this.props.highlight}
          error={this.props.error}
          flat={this.props.flat}
          selected={this.state.focused}
          name={this.props.name}
          inline={this.props.inline}
          autoFocus={this.props.autoFocus}
          {...this.getValidators()}
        />
        {
          this.props.floatingLabel &&
          <InputLabel
            floating={this.isLabelFloating()}
            selected={this.state.focused}
            error={this.props.error}
            highlight={this.props.highlight}
            condensed={this.props.condensed}
          >
            {this.getLabel()}
          </InputLabel>
        }
      </InputFieldContainer>
    );
  }
}

export default Input;

