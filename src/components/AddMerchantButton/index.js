import React from 'react';
import styled from 'styled-components';
import { SecondaryButton } from 'styles/buttons';

const AddBtn = styled(SecondaryButton)`
  position: fixed;
  bottom: 25px;
  right: 35px;
  border-radius: 50%;
  width: 4rem;
  height: 4rem;
  font-size: 1.75rem;
`;

function AddMerchantButton({ onClick, className }) {
  return (
    <AddBtn onClick={onClick} className={className}>+</AddBtn>
  );
}

export default AddMerchantButton;
