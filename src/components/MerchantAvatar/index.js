import React from 'react';
import styled from 'styled-components';

const Img = styled.img`
  width: 100%;
  border-radius: 50%;
`;

function MerchantAvatar(props) {
  return (
    <Img src={props.avatar} />
  );
}

MerchantAvatar.defaultProps = {
  src: '//via.placeholder.com/150x150',
};

export default MerchantAvatar;
