import React from 'react';
import styled from 'styled-components';
import MerchantAvatar from 'components/MerchantAvatar';

const Wrapper = styled.div`
  position: relative;
`;

const UploadButton = styled.a`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
`;

class UploadAvatar extends React.PureComponent {
  handleUploadClick = () => {
    this.inputEl.click();
  }

  handleChange = (e) => {
    this.props.onChange(URL.createObjectURL(e.target.files[0]));
  }

  render() {
    return (
      <Wrapper>
        <MerchantAvatar avatar={this.props.avatar} />
        <UploadButton onClick={this.handleUploadClick}>Upload</UploadButton>
        <input type="file" ref={(el) => {this.inputEl = el}} style={{visibility: 'hidden'}} onChange={this.handleChange} />
      </Wrapper>
    )
  }
}

export default UploadAvatar;
