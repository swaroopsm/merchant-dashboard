import React from 'react';
const icons = require.context('../../assets', false, /\.\/icon-(?:.+)\.svg/);

const getDimensions = (width, height) => {
  if (width && height) {
    return { width, height };
  } else if (width) {
    return { width, height: width };
  } else if (height) {
    return { width: height, height };
  }

  return { width: '1.5rem' };
};


export default function Icon({ icon, className, onClick, ...otherProps }) {
  const SvgIcon = icons(`./icon-${icon}.svg`);
  const { width, height } = otherProps;
  const style = { ...getDimensions(width, height), ...otherProps.style };

  return (
    <SvgIcon
      style={style}
      className={className}
      onClick={onClick}
    />
  );
}
