import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Row, Col } from 'react-flexbox-grid';
import { SecondaryButton } from 'styles/buttons';

const InfoText = styled.span`
  margin-left: 8px;
  margin-right: 8px;
`;

const Wrapper = styled(Col)`
  background: white;
  padding: 12px;
  text-align: center;
`;

class Paginator extends React.PureComponent {
  static propTypes = {
    totalPages: PropTypes.number,
    currentPage: PropTypes.number,
    onNext: PropTypes.func,
    onPrev: PropTypes.func,
  };

  render() {
    if (this.props.totalPages === 1) {
      return (
        <Row >
          <Col mdOffset={4} sm={12} md={4}>
            <InfoText>{this.props.currentPage} of {this.props.totalPages}</InfoText>
          </Col>
        </Row>
      )
    }

    return (
      <Row>
        <Wrapper mdOffset={4} xs={11} md={4}>
          <SecondaryButton disabled={this.props.currentPage === 1} onClick={this.props.onPrev}>&laquo; Previous</SecondaryButton>
          <InfoText>{this.props.currentPage} of {this.props.totalPages}</InfoText>
          <SecondaryButton disabled={this.props.currentPage === this.props.totalPages} onClick={this.props.onNext}>Next &raquo;</SecondaryButton>
        </Wrapper>
      </Row>
    )
  }
}

export default Paginator;
