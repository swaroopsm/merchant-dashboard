import React from 'react';
// import PropTypes from 'prop-types';
import { Card, BackLitText } from 'styles/commons';
import { $grayLight, $fontSizeSm } from 'styles/variables';
import { PrimaryButton } from 'styles/buttons';
import styled from 'styled-components';
import MerchantAvatar from 'components/MerchantAvatar';
import { Col, Row } from 'react-flexbox-grid';
import Icon from 'components/Icon';
import BidItem from 'components/BidItem';

const ActionBlock = styled.div`
  display: inline-block;
  float: right;

  > button:first-child {
    margin-right: 4px;
  }
`;

const Header = styled.div`
  padding-top: 10px;
  padding-bottom: 10px;
  border-bottom: 1px solid ${$grayLight};
  margin-bottom: 10px;
  display: flex;
  width: 100%;
`;

const MerchantName = styled.h3`
  font-size: ${$fontSizeSm};
  padding-bottom: 12px;
`;

const PremiumBadge = styled(Icon)`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
`;

const MerchantCard = styled(Card)`
  overflow: hidden;
`;

const Body = styled.div`

`;

const AvatarWrapper = styled.div`
  position: relative;
`;

const DetailCol = styled(Col)`
  margin-bottom: 8px;
`;

const ShowMore = styled(DetailCol)`
  display: flex;
  align-items: center;
  justify-content: center;
`;


class MerchantItem extends React.PureComponent {
  handleEdit = () => {
    this.props.onEdit(this.props.merchant.id);
  }

  handleRemove = () => {
    this.props.onRemove(this.props.merchant.id);
  }

  handleShowMore = () => {
    this.props.onShowMoreBids(this.props.merchant.id);
  }

  render() {
    const { merchant } = this.props;

    return (
      <MerchantCard>
        <BackLitText>Merchant Id: {merchant.id}</BackLitText>
        <ActionBlock>
          <PrimaryButton invert size="sm" onClick={this.handleRemove}>Delete</PrimaryButton>
          <PrimaryButton size="sm" onClick={this.handleEdit}>Edit</PrimaryButton>
        </ActionBlock>
        <Header between="xs">
          <Col xs={3} md={2}>
            <AvatarWrapper>
              <MerchantAvatar avatar={merchant.avatarUrl} />
              { merchant.hasPremium && <PremiumBadge icon="premium-badge" /> }
            </AvatarWrapper>
          </Col>
          <Col xs={9} md={10}>
            <MerchantName>{merchant.firstname} {merchant.lastname}</MerchantName>
            <Row>
              <Col xs={1}>
                <Icon icon='email' />
              </Col>
              <Col xs={11}>
                <p><a href={`mailto:${merchant.email}`}>{merchant.email}</a></p>
              </Col>
            </Row>

            <Row>
              <Col xs={1}>
                <Icon icon='phone' />
              </Col>
              <Col xs={11}>
                <p><a href={ `tel:${merchant.phone}` }>{merchant.phone}</a></p>
              </Col>
            </Row>

            {/*
              <Row>
                <Col xs={12}>
                  <SecondaryButton invert size="sm">Total Bids: {merchant.bids.length}</SecondaryButton>
                </Col>
              </Row>
            */}
          </Col>
        </Header>
        <Body>
          <Row>
            <Col xs={12}>
              <h4>{ merchant.bids && merchant.bids.length > 0 ? 'Previous Bids' : 'No bids havve been made yet!' }</h4>
            </Col>
          </Row>
          <Row>
            {
              merchant.bids && merchant.bids.length > 0 && merchant.bids.map((bid) => (
                <DetailCol key={bid.id} xs={12} md={5}>
                  <BidItem bid={bid} />
                </DetailCol>
              ))
            }
            {
              merchant.showMoreBids && (
                <ShowMore xs={12} md={2}>
                  <a onClick={this.handleShowMore}>More &raquo;</a>
                </ShowMore>
              )
            }
          </Row>
        </Body>
      </MerchantCard>
    );
  }
}

export default MerchantItem;
