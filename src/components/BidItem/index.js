import React from 'react';
import styled from 'styled-components';
import { BackLitText, Card } from 'styles/commons';
import { $brandSecondary } from 'styles/variables';
import formatDate from 'date-fns/format';

const BidCard = styled(Card)`
`;

const BidPrice = styled.p`
  color: ${$brandSecondary};
`;

const CarTitle = styled.p`

`;

const CreatedDate = styled.small`
  text-transform: uppercase;
  display: inline-block;
  padding-top: 12px;
`;

const BidDetails = styled.div`
  padding-top: 4px;
  padding-bottom: 4px;
`;

function BidItem({ bid }) {
  return (
    <BidCard>
      <BackLitText>Bid Id: {bid.id}</BackLitText>
      <BidDetails>
        <CarTitle>{bid.carTitle}</CarTitle>
        <BidPrice>€ {bid.amount}</BidPrice>
        <CreatedDate>Created: {formatDate(bid.created, 'D MMM YYYY')}</CreatedDate>
      </BidDetails>
    </BidCard>
  );
}

export default BidItem;
