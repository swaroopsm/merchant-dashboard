import React from 'react';
import { FormItem, FormActions } from 'styles/commons';
import InputField from 'components/Input';
import { PrimaryButton } from 'styles/buttons';
import { Col, Row } from 'react-flexbox-grid';
import createForm from 'hoc/createForm';
import get from 'lodash/get';
import validateRequired from 'validators/required';
import validateEmail from 'validators/email';
import UploadAvatar from 'components/UploadAvatar';
import Icon from 'components/Icon';

class MerchantForm extends React.PureComponent {

  getError(field) {
    return get(this.props, `errors.${field}`, '');
  }

  getValue(field) {
    return get(this.props, `values.${field}`, '');
  }

  setAvatar = (url) => {
    this.props.onSetField('avatarUrl', url);
  }

  onChangeCheckbox = (e) => {
    this.props.onSetField(e.target.name, e.target.checked);
  }

  render() {
    const { onChange, onSubmit } = this.props;

    return (
      <form onSubmit={onSubmit} noValidate>
        <FormItem>
          <Row center="xs">
            <Col xs={4} sm={3} md={2}>
              <UploadAvatar avatar={this.getValue('avatarUrl')} onChange={this.setAvatar} name="avatarUrl" />
            </Col>
          </Row>
        </FormItem>

        <FormItem>
          <InputField
            name="firstname"
            label="Firstname"
            onChange={onChange}
            value={this.getValue('firstname')}
            error={this.getError('firstname')}
          />
        </FormItem>

        <FormItem>
          <InputField
            name="lastname"
            label="Lastname"
            onChange={onChange}
            value={this.getValue('lastname')}
            error={this.getError('lastname')}
          />
        </FormItem>

        <FormItem>
          <InputField
            name="email"
            label="Email"
            type="email"
            onChange={onChange}
            value={this.getValue('email')}
            error={this.getError('email')}
          />
        </FormItem>

        <FormItem>
          <InputField
            name="phone"
            label="Phone"
            onChange={onChange}
            value={this.getValue('phone')}
            error={this.getError('phone')}
          />
        </FormItem>

        <FormItem>
          <input
            type="checkbox"
            id="premium-member"
            name="hasPremium"
            onChange={this.onChangeCheckbox}
            value={this.getValue('hasPremium')}
            error={this.getError('hasPremium')}
            checked={this.getValue('hasPremium')}
          />
          <label htmlFor="premium-member"><Icon icon="premium-badge" width='2.5rem' /></label>
        </FormItem>

        <FormActions>
          <Row end="xs">
            <Col xs={4}><PrimaryButton fluid disabled={this.props.hasErrors}>Submit</PrimaryButton></Col>
          </Row>
        </FormActions>
      </form>
    );
  }
}

export default createForm({
  initialValues: {
    firstname: '',
    lastname: '',
    email: '',
    phone: '',
    avatarUrl: '',
    hasPremium: false,
  },
  validate: {
    firstname(value) {
      if (!validateRequired(value)) { return 'Firstname cannot be empty'; }

      return false;
    },
    lastname(value) {
      if (!validateRequired(value)) { return 'Lastname cannot be empty'; }

      return false;
    },
    email(value) {
      if (!validateRequired(value)) { return 'Email cannot be empty'; }
      if (!validateEmail(value)) { return 'Email seems to be invalid'; }

      return false;
    },
    phone(value) {
      if (!validateRequired(value)) { return 'Phone cannot be empty'; }

      return false;
    }
  },
})(MerchantForm);
