import React, { PureComponent } from 'react';
import ReactModal from 'react-modal';
import styled from 'styled-components';
import { $brandPrimary, $brandSecondary } from 'styles/variables';

const ModalWrapper = styled.div`
`;

const ModalHeader = styled.div`
  background: ${({ type }) => {
    if (type === 'primary') { return $brandPrimary; }
    if (type === 'secondary') { return $brandSecondary; }

    return 'white';
  }};
  position: relative;
  padding: 20px;
`;

const ModalTitle = styled.h4`
  text-align: center;
  color: white;
  font-weight: 500;
  text-transform: uppercase;
}
`;

export const ModalBody = styled.div`
  padding: 10px 20px;
  max-height: 80vh;
  overflow-x: hidden;
  overflow-y: auto;
`;

export const ModalCloseIcon = styled.div`
  position: absolute;
  color: white;
  cursor: pointer;
  right: 10px;
  top: 10px;
  font-size: 2rem;
`;

class Modal extends PureComponent {
  static defaultProps = {
    isOpen: false,
    size: 'md',
    closeable: true,
    closeOnOverlayClick: true,
    highlightTitle: true,
    closeOnEsc: true,
    showHeader: true,
    animate: false,
    type: 'primary',
  };

  componentWillReceiveProps(prevProps, nextProps) {
    if (prevProps.isOpen && nextProps.isOpen === false) {
      this.resetCloseTimer();
    }
  }

  resetCloseTimer() {
    if (this.timer) { clearTimeout(this.timer); }
  }

  startCloseTimer() {
    if (this.props.onAfterClose) {
      this.timer = setTimeout(this.props.onAfterClose, 500);
    }
  }

  closeModal = (e) => {
    if (!this.props.closeable) { return null; }

    this.resetCloseTimer();
    this.startCloseTimer();

    if (e.type === 'keydown' && e.keyCode === 27) {
      if (this.props.closeOnEsc) {
        return this.props.onClose();
      }
    }

    return this.props.onClose();
  }

  render() {
    return (
      <ReactModal
        contentLabel={this.props.title || 'Unnamed Modal'}
        isOpen={this.props.isOpen}
        onRequestClose={this.closeModal}
        shouldCloseOnOverlayClick={this.props.closeOnOverlayClick}
        closeTimeoutMS={500}
        className={{
          base: ['modal-base', this.props.animate ? '' : 'no-animate'].join(' '),
          afterOpen: ['modal-base--after-open', this.props.stickyTop ? 'modal-base--sticky-top' : ''].join(' '),
          beforeClose: 'modal-base--before-close',
        }}
        overlayClassName={{
          base: 'modal-overlay',
          afterOpen: 'modal-overlay--after-open',
          beforeClose: 'modal-overlay--before-close',
        }}
      >
        <ModalWrapper
          size={this.props.size}
          className={this.props.className}
        >
          {
            this.props.showHeader &&
            <ModalHeader
              showTitle={!!this.props.title}
              type={this.props.type}
            >
              {
                <ModalTitle showEmptyContent={!!this.props.title}>
                  {this.props.title}
                  {this.props.closeable && <ModalCloseIcon onClick={this.closeModal}>&times;</ModalCloseIcon> }
                </ModalTitle>
              }
            </ModalHeader>
          }
          <ModalBody>
            {this.props.children}
          </ModalBody>
        </ModalWrapper>
      </ReactModal>
    );
  }
}

export default Modal;

