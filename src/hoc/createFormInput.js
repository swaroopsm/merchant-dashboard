import React from 'react';

export default function createFormInputHOC(options = {}) {
  return function createFormInput(WrappedComponent) {
    return class FormInputHOC extends React.PureComponent {
      constructor(props) {
        super(props);

        this.state = {
          value: '',
        }
      }

      componentWllMount() {
        if (this.props.value) {
          this.setState({ value: this.props.value });
        }
      }

      componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.props.value) {
          this.setState({ value: this.props.value });
        }
      }

      handleChange = (e, ...rest) => {
        this.setState({ value: e.target.value });

        if (this.props.onChange) {
          this.props.onChange(e, ...rest);
        }
      }

      render() {
        return (
          <WrappedComponent
            {...this.props}
            value={this.state.value}
            onChange={this.handleChange}
          />
        )
      }
    }
  }
}
