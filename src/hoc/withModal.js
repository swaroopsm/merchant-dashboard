import React from 'react';

export default function withModalHOC(options = {}) {
  return function withModal(WrappedComponent) {
    return class ModalHOC extends React.PureComponent {
      constructor(props) {
        super(props);

        const activeModals = [];
        let state = {};

        Object.keys(options).forEach((option) => {
          state = { ...state, [`${option}Toggle`]: false };
          activeModals.push(option);
        });

        this.state = state;
        this.activeModals = activeModals;
      }

      modalFn = () => {
      }

      handleModalToggle = (modalKey, toOpen) => {
        const key = `${modalKey}Toggle`;

        return () => {
          this.setState({
            [key]: toOpen,
          });
        };
      }

      getActiveModals() {
        let modals = {};

        this.activeModals.forEach((modal) => {
          modals = {
            ...modals,
            [`${modal}Opened`]: this.state[`${modal}Toggle`],
            [`${modal}Open`]: this.handleModalToggle(modal, true),
            [`${modal}Close`]: this.handleModalToggle(modal, false),
          };
        });

        return modals;
      }

      render() {
        return (
          <WrappedComponent
            {...this.props}
            {...this.getActiveModals()}
            onModalToggle={this.handleModalToggle}
          />
        );
      }
    };
  };
}

