import React from 'react';

export default function createFormHOC(options = {}) {
  return function createForm(WrappedComponent) {
    return class FormHOC extends React.PureComponent {
      constructor(props) {
        super(props);

        this.state = {
          values: this.setValues(),
          errors: {},
        };
      }

      setValues() {
        return options.initialValues || {};
      }

      validateForErrors() {
        if (options.validate) {
          const errors = Object.keys(options.validate)
            .reduce((initial, current) => {
              const error = this.validateFieldForErrors(current, this.state.values[current]);

              if (error) { return { ...initial, [current]: error }; }

              return initial;
            }, {});

          return Object.keys(errors).length > 0 ? errors : false;
        }

        return false;
      }

      validateFieldForErrors(field, value) {
        if (options.validate && options.validate[field]) {
          return options.validate[field](value);
        }
      }

      handleSubmit = (e) => {
        e.preventDefault();

        const errors = this.validateForErrors();

        if (errors) {
          return this.setState({ errors });
        }

        this.props.onSubmit && this.props.onSubmit(this.state.values);
      };

      handleChange = (key, value) => {
        const values = { ...this.state.values, [key]: value };
        let errors = { ...this.state.errors };
        const fieldError = this.validateFieldForErrors(key, value);

        if (fieldError) {
          errors[key] = fieldError;
        } else {
          // Mutations
          // Not the best
          delete errors[key];
        }

        this.setState({ values, errors });
      }

      handleInputChange = (e) => {
        this.handleChange(e.target.name, e.target.value);
      }

      componentWillMount() {
        if (this.props.initialValues) {
          this.setState({ values: this.props.initialValues });
        }
      }

      render() {
        return (
          <WrappedComponent
            {...this.props}
            onSubmit={this.handleSubmit}
            onChange={this.handleInputChange}
            onSetField={this.handleChange}
            errors={this.state.errors}
            hasErrors={this.state.errors && Object.keys(this.state.errors).length > 0}
            values={this.state.values}
          />
        )
      }
    }
  }
}
