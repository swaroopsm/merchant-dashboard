export default function(value) {
  if (value) {
    if (value.trim().length > 0) {
      return true;
    }

    return false;
  }

  return false;
}
