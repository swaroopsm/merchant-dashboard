export const $brandPrimary = '#1B4691';
export const $brandSecondary = '#F6852B';
export const $brandError = '#E23D2D';


// Grayscales
export const $blackLight = '#9d9d9d';
export const $blackDark = '#5b5b5b';
export const $grayLight = '#eeeeee';
export const $grayLightest = '#fafafa';
export const $grayDark = '#D2D2D2';

// Fonts
export const $fontSizeBase = '1rem';
export const $fontSizeXlg = '3.157rem';
export const $fontSizeLg = '2.369rem';
export const $fontSizeMd = '1.777rem';
export const $fontSizeSm = '1.333rem';
export const $fontSizeXs = '0.75rem';
export const $fontSizeXxs = '0.563rem';

// Font Weights
export const $fontWeightBold = 600;
export const $fontWeightMedium = 500;

// Forms
export const $inputBg = 'rgba(250, 250, 250, 0.04)';
export const $inputBorderColor = '#DFDFDF';
export const $inputErrorBorderColor = $brandError;
export const $inputSelectedBorderColor = $grayDark;
export const $inputHighlightSelectedBorderColor = $brandPrimary;
export const $inputLabelColor = $grayDark;
export const $inputBoxShadowColor = 'rgba(0, 0, 0, 0.15)';
export const $inputBoxHeight = '48px';
