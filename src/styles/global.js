import { injectGlobal } from 'styled-components';
import { $brandPrimary } from 'styles/variables';

injectGlobal`
  a {
    color: ${$brandPrimary};
  }
`
