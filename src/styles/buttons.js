import {
  $brandPrimary,
  $brandSecondary,
  $fontSizeXlg,
  $fontSizeLg,
  $fontSizeMd,
  $fontSizeBase,
  $fontSizeXs,
} from './variables';
import styled from 'styled-components';

const createButton = (options = {}, otherStyles) => styled.button`
  background: ${({ invert }) => invert ? 'white' : options.bg};
  color: ${({ invert }) => invert ? options.bg : 'white'};
  outline: none;
  color: ${({ invert }) => invert ? options.bg : 'white'};
  border: 1px solid ${options.bg};
  ${otherStyles};
  cursor: pointer;
  height: ${(props) => {
    if (props.lg) { return $fontSizeXlg }
    if (props.sm) { return $fontSizeMd }

    return $fontSizeLg;
  }};
  padding: 0.375rem 0.5rem;
  text-transform: uppercase;
  font-size: ${({ size }) => {
    if (size === 'sm') { return $fontSizeXs }

    return $fontSizeBase;
  }};
  ${({ fluid }) => fluid ? 'width: 100%' : null};

  &:disabled {
    opacity: 0.5;
  }
`;

export const PrimaryButton = createButton({
  bg: $brandPrimary,
});

export const SecondaryButton = createButton({
  bg: $brandSecondary,
});
