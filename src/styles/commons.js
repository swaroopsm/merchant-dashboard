import styled from 'styled-components';
import { $grayDark, $blackDark, $grayLight, $fontSizeXs } from './variables';

export const Card = styled.div`
  background: white;
  padding: 8px 10px;
  box-shadow: 0px 2px 8px 0px ${$grayDark};
`;

export const BackLitText = styled.span`
  background: ${$grayLight};
  color: ${$blackDark};
  font-style: italic;
  text-transform: uppercase;
  padding: 4px;
  font-size: ${$fontSizeXs};
`;

export const FormItem = styled.div`
  margin-bottom: 12px;
`;

export const FormActions = styled.div`
  padding-top: 20px;
  padding-botton: 20px;
  border-top: 1px solid ${$grayLight};
  margin-bottom: 12px;
`;
