import { merchantItemNormalizer } from 'schemas';
import { createMerchant, updateMerchant as updateMerchantService } from 'services/merchants';
import {
  ADD_MERCHANT,
  ADD_MERCHANT_SUCCESS,
  EDIT_MERCHANT,
  EDIT_MERCHANT_SUCCESS,
} from './constants';

export function addMerchant(payload) {
  return ((dispatch) => {
    dispatch({ type: ADD_MERCHANT, payload });

    return createMerchant(payload)
      .then((merchant) => {
        dispatch(finishAddingMerchant(merchant));
      });
  });
}

export function finishAddingMerchant(merchant) {
  return { type: ADD_MERCHANT_SUCCESS, ...(merchantItemNormalizer(merchant)) };
}

export function editMerchant(merchantId) {
  return { type: EDIT_MERCHANT, merchantId };
}

export function updateMerchant(payload) {
  return ((dispatch) => {
    return updateMerchantService(payload)
      .then((merchant) => {
        dispatch(finishEditingMerchant(merchant));
      });
  });
}

export function finishEditingMerchant(merchant) {
  return { type: EDIT_MERCHANT_SUCCESS, ...(merchantItemNormalizer(merchant)) };
}
