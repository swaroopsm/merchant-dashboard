export const ADD_MERCHANT = 'merchants/ADD_MERCHANT';
export const ADD_MERCHANT_SUCCESS = 'merchants/ADD_MERCHANT_SUCCESS';

export const EDIT_MERCHANT = 'merchants/EDIT_MERCHANT';
export const EDIT_MERCHANT_SUCCESS = 'merchants/EDIT_MERCHANT_SUCCESS';
