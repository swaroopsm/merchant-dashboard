import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { selectModal, selectActiveMerchant } from './selectors';
import Modal from 'components/Modal';
import MerchantForm from 'components/MerchantForm';
import { addMerchant, updateMerchant } from './actions';

class MerchantFormModal extends React.PureComponent {
  handleMerchantFormSubmit = (payload) => {
    if (this.props.activeMerchant) {
      this.props.updateMerchant(payload);
    } else {
      this.props.addMerchant(payload);
    }
  }

  render() {
    return (
      <Modal
        isOpen={this.props.isOpen}
        onClose={this.props.onClose}
        title={this.props.title}
      >
        <MerchantForm
          initialValues={this.props.activeMerchant}
          onSubmit={this.handleMerchantFormSubmit}
        />
      </Modal>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  merchantModal: selectModal(),
  activeMerchant: selectActiveMerchant(),
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  addMerchant,
  updateMerchant,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MerchantFormModal);
