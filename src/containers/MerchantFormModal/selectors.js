import { createSelector } from 'reselect';
import { selectActiveMerchantIdState, selectMerchantsState } from 'containers/MerchantList/selectors';
import get from 'lodash/get';

const selectMerchantModalState = () => (state) => get(state, 'merchantList.showMerchantModal');

export const selectModal = () => createSelector(
  selectMerchantModalState(),
  selectActiveMerchantIdState(),
  (isModalOpen, activeMerchantId) => ({
    isOpen: isModalOpen,
    title: activeMerchantId ? 'Edit Merchant' : 'Add Merchant',
  })
);

export const selectActiveMerchant = () => createSelector(
  selectActiveMerchantIdState(),
  selectMerchantsState(),
  (id, merchants) => merchants[id]
);
