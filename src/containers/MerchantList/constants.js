export const FETCH_MERCHANTS = 'merchantList/FETCH_MERCHANTS';
export const FETCH_MERCHANTS_SUCCESS = 'merchantList/FETCH_MERCHANTS_SUCCESS';
export const MIN_BIDS_IN_CARD_COUNT = 2;
export const SHOW_MERCHANT_FORM = 'merchants/SHOW_MERCHANT_FORM';
export const HIDE_MERCHANT_FORM = 'merchants/HIDE_MERCHANT_FORM';

export const REMOVE_MERCHANT = 'merchants/REMOVE_MERCHANT';
export const REMOVE_MERCHANT_SUCCESS = 'merchants/REMOVE_MERCHANT_SUCCESS';

export const OPEN_BIDS_MODAL = 'merchants/OPEN_BIDS_MODAL';
export const CLOSE_BIDS_MODAL = 'merchants/CLOSE_BIDS_MODAL';

export const LOAD_NEXT_PAGE = 'merchants/LOAD_NEXT_PAGE';
export const LOAD_NEXT_PAGE_SUCCESS = 'merchants/LOAD_NEXT_PAGE_SUCCESS';
export const LOAD_PREV_PAGE = 'merchants/LOAD_PREV_PAGE';
export const LOAD_PREV_PAGE_SUCCESS = 'merchants/LOAD_PREV_PAGE_SUCCESS';
