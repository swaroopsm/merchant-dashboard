import { FETCH_MERCHANTS_SUCCESS, FETCH_MERCHANTS } from '../constants';
import {
  fetchMerchants,
  finishFetchingMerchants,
} from '../actions';
import { configureMockStore } from 'testutils';

jest.mock('services/merchants', () => ({
  getMerchantList: jest.fn().mockImplementation(() => new Promise((resolve, reject) => { resolve([]) })),
}));

jest.mock('schemas', () => ({
  merchantListNormalizer: jest.fn().mockImplementation(() => ({ result: [] })),
}));

let store;

beforeEach(() => {
  store = configureMockStore({});
});

describe('fetchMerchants', () => {
  it('should fetch merchants', () => {
    return store.dispatch(fetchMerchants())
      .then(() => {
        const actions = store.getActions();
        
        expect(actions[0]).toEqual({ type: FETCH_MERCHANTS });
        expect(actions[1]).toEqual(finishFetchingMerchants({ merchants: [] }));
      });
  });
});

describe('finishFetchingMerchants', () => {
  it('should finish fetching merchants', () => {
    expect(finishFetchingMerchants({ total: 0, merchants: [] })).toEqual({
      type: FETCH_MERCHANTS_SUCCESS,
      data: {result: []},
      totalResults: 0,
    });
  });
});
