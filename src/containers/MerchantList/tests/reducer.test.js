import merchantListReducer from '../reducer';
import { FETCH_MERCHANTS } from '../constants';

describe('merchantListReducer', () => {
  let state;

  beforeEach(() => {
    state = {
      isLoading: false,
      showMerchantModal: false,
      showMerchantForm: false,
      activeMerchantId: null,
      entities: {},
      merchants: [],
      showBidsModal: false,
      perPage: 18,
      currentPage: -1,
      totalResults: 0,
    };
  });

  it('returns the initial state correctly', () => {
    expect(merchantListReducer(undefined, {})).toEqual(state);
  });

  it('responds to fetch merchants correctly', () => {
    expect(merchantListReducer(state, {type: FETCH_MERCHANTS})).toEqual({
      isLoading: true,
      showMerchantModal: false,
      showMerchantForm: false,
      activeMerchantId: null,
      entities: {},
      merchants: [],
      showBidsModal: false,
      perPage: 18,
      currentPage: 0,
      totalResults: 0,
    })
  });
});
