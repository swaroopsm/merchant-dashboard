import { getMerchantList, deleteMerchant } from 'services/merchants';
import { merchantListNormalizer } from 'schemas';
import {
  FETCH_MERCHANTS_SUCCESS,
  FETCH_MERCHANTS,
  SHOW_MERCHANT_FORM,
  HIDE_MERCHANT_FORM,
  REMOVE_MERCHANT,
  REMOVE_MERCHANT_SUCCESS,
  OPEN_BIDS_MODAL,
  CLOSE_BIDS_MODAL,
  LOAD_NEXT_PAGE,
  LOAD_PREV_PAGE,
} from './constants';
import { selectPageFrom, selectPageTo } from './selectors';

const loadMerchantData = (dispatch, getState) => {
  const state = getState();
  const from = selectPageFrom()(state);
  const to = selectPageTo()(state);

  return getMerchantList({ from, to })
    .then((data) => {
      dispatch(finishFetchingMerchants(data));
    });
};

export function fetchMerchants() {
  return ((dispatch, getState) => {
    dispatch({type: FETCH_MERCHANTS});

    return loadMerchantData(dispatch, getState);
  });
}

export function finishFetchingMerchants(data) {
  return {
    type: FETCH_MERCHANTS_SUCCESS,
    data: merchantListNormalizer(data.merchants),
    totalResults: data.total,
  };
}

export function showMerchantForm(merchantId = null) {
  return { type: SHOW_MERCHANT_FORM, merchantId };
}

export function hideMerchantFormModal() {
  return { type: HIDE_MERCHANT_FORM };
}

export function removeMerchant(merchantId) {
  return ((dispatch) => {
    dispatch({type: REMOVE_MERCHANT});

    return deleteMerchant(merchantId)
      .then(() => {
        dispatch(finishRemovingMerchant(merchantId));
      });
  });
}

export function finishRemovingMerchant(merchantId) {
  return { type: REMOVE_MERCHANT_SUCCESS, merchantId };
}

export function openBidsModal(merchantId) {
  return { type: OPEN_BIDS_MODAL, merchantId };
}

export function closeBidsModal() {
  return { type: CLOSE_BIDS_MODAL };
}

export function loadNextPage() {
  return (dispatch, getState) => {
    dispatch({ type: LOAD_NEXT_PAGE });

    return loadMerchantData(dispatch, getState);
  };
}

export function loadPrevPage() {
  return (dispatch, getState) => {
    dispatch({ type: LOAD_PREV_PAGE });

    return loadMerchantData(dispatch, getState);
  };
}
