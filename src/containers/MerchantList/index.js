import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchMerchants, showMerchantForm, hideMerchantFormModal, removeMerchant, openBidsModal, closeBidsModal, loadNextPage, loadPrevPage } from './actions';
import { createStructuredSelector } from 'reselect';
import { selectMerchants, selectModal, selectBidsModal, selectPaginator, selectIsLoading } from './selectors';
import MerchantList from 'components/MerchantList';
import AddMerchantButton from 'components/AddMerchantButton';
import styled from 'styled-components';
import MerchantFormModal from 'containers/MerchantFormModal';
import { editMerchant } from 'containers/MerchantFormModal/actions';
import BidsModal from 'components/BidsModal';
import Paginator from 'components/Paginator';

const Wrapper = styled.div``;

class MerchantListContainer extends React.PureComponent {
  componentDidMount() {
    this.props.fetchMerchants();
  }

  showMerchantForm = () => {
    this.props.showMerchantForm();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isLoading === true && this.props.isLoading === false) {
      window.scrollTo(0, 0);
    }
  }

  render() {
    return (
      <Wrapper>
        <MerchantList
          merchants={this.props.merchants}
          onEdit={this.props.editMerchant}
          onRemove={this.props.removeMerchant}
          onClick={this.props.openBidsModal}
          onOpenBids={this.props.openBidsModal}
        />
        <Paginator
          {...this.props.paginator}
          onNext={this.props.loadNextPage}
          onPrev={this.props.loadPrevPage}
        />
        <AddMerchantButton onClick={this.showMerchantForm} />
        <MerchantFormModal {...this.props.merchantModal} onClose={this.props.hideMerchantFormModal} />
        <BidsModal {...this.props.bidsModal} onClose={this.props.closeBidsModal} />
      </Wrapper>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  merchants: selectMerchants(),
  merchantModal: selectModal(),
  bidsModal: selectBidsModal(),
  paginator: selectPaginator(),
  isLoading: selectIsLoading(),
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  fetchMerchants,
  showMerchantForm,
  hideMerchantFormModal,
  editMerchant,
  removeMerchant,
  openBidsModal,
  closeBidsModal,
  loadNextPage,
  loadPrevPage,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MerchantListContainer);
