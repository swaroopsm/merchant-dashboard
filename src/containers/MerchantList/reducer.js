import {
  FETCH_MERCHANTS,
  FETCH_MERCHANTS_SUCCESS,
  SHOW_MERCHANT_FORM,
  HIDE_MERCHANT_FORM,
  REMOVE_MERCHANT_SUCCESS,
  OPEN_BIDS_MODAL,
  CLOSE_BIDS_MODAL,
  LOAD_NEXT_PAGE,
  LOAD_PREV_PAGE,
} from './constants';
import { ADD_MERCHANT, ADD_MERCHANT_SUCCESS, EDIT_MERCHANT, EDIT_MERCHANT_SUCCESS } from 'containers/MerchantFormModal/constants';
import merge from 'lodash/merge';
import omit from 'lodash/omit';

const initialState = {
  isLoading: false,
  showMerchantModal: false,
  showMerchantForm: false,
  activeMerchantId: null,
  entities: {},
  merchants: [],
  showBidsModal: false,
  perPage: 18,
  currentPage: -1,
  totalResults: 0,
};

export default function merchantListReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_MERCHANTS: {
      return {
        ...state,
        isLoading: true,
        currentPage: state.currentPage + 1,
      };
    }

    case FETCH_MERCHANTS_SUCCESS: {
      return {
        ...state,
        entities: action.data.entities,
        merchants: action.data.result,
        isLoading: false,
        totalResults: action.totalResults,
      };
    }

    case LOAD_NEXT_PAGE: {
      return {
        ...state,
        isLoading: true,
        currentPage: state.currentPage + 1,
      }
    }

    case LOAD_PREV_PAGE: {
      return {
        ...state,
        isLoading: true,
        currentPage: state.currentPage - 1,
      }
    }

    case ADD_MERCHANT: {
      return {
        ...state,
      };
    }

    case EDIT_MERCHANT: {
      return {
        ...state,
        showMerchantModal: true,
        activeMerchantId: action.merchantId,
      };
    }

    case EDIT_MERCHANT_SUCCESS: {
      const merchant = state.entities.merchants[action.result];
      const newMerchant = merge({}, merchant, action.entities.merchants[action.result]);
      const newMerchants = { ...state.entities.merchants, [action.result]: newMerchant };

      return {
        ...state,
        activeMerchantId: null,
        showMerchantModal: false,
        entities: { ...state.entities, merchants: newMerchants },
      };
    }

    case ADD_MERCHANT_SUCCESS: {
      return {
        ...state,
        showMerchantModal: false,
        showMerchantForm: false,
        entities: merge(state.entities, action.entities),
        merchants: [action.result].concat(state.merchants),
        activeMerchantId: null,
      };
    }

    case REMOVE_MERCHANT_SUCCESS: {
      const merchant = state.entities.merchants[action.merchantId];
      const newMerchants = omit(state.entities.merchants, action.merchantId);
      const newBids = omit(state.entities.bids, merchant.bids);
      const newResultSlice = state.merchants.concat([]);

      newResultSlice.splice(state.merchants.indexOf(action.merchantId), 1);

      return {
        ...state,
        entities: {
          merchants: newMerchants,
          bids: newBids,
        },
        merchants: newResultSlice,
      };
    }

    case SHOW_MERCHANT_FORM: {
      return {
        ...state,
        showMerchantModal: true,
        showMerchantForm: true,
        activeMerchantId: action.merchantId,
      };
    }

    case HIDE_MERCHANT_FORM: {
      return {
        ...state,
        showMerchantModal: false,
      };
    }

    case OPEN_BIDS_MODAL: {
      return {
        ...state,
        activeMerchantId: action.merchantId,
        showBidsModal: true,
      };
    }

    case CLOSE_BIDS_MODAL: {
      return {
        ...state,
        showBidsModal: false,
      };
    }

    default: return state;
  }
}
