import { createSelector } from 'reselect';
import get from 'lodash/get';
import take from 'lodash/take';
import { MIN_BIDS_IN_CARD_COUNT } from './constants';

const selectMerchantModalState = () => (state) => get(state, 'merchantList.showMerchantModal');
const selectBidsModalState = () => (state) => get(state, 'merchantList.showBidsModal');
const selectPerPage = () => (state) => get(state, 'merchantList.perPage');
const selectTotalResults = () => (state) => get(state, 'merchantList.totalResults');
const selectCurrentPage = () => (state) => get(state, 'merchantList.currentPage');

export const selectMerchantsState = () => (state) => get(state, 'merchantList.entities.merchants', {});
export const selectBidsState = () => (state) => get(state, 'merchantList.entities.bids', {});
export const selectMerchantsListState = () => (state) => get(state, 'merchantList.merchants', []);
export const selectActiveMerchantIdState = () => (state) => get(state, 'merchantList.activeMerchantId');
export const selectIsLoading = () => (state) => get(state, 'merchantList.isLoading');

export const selectPageFrom = () => createSelector(
  selectCurrentPage(),
  selectPerPage(),
  (currentPage, perPage) => currentPage * perPage
);

export const selectPageTo = () => createSelector(
  selectPageFrom(),
  selectPerPage(),
  (pageFrom, perPage) => pageFrom + perPage
);

export const selectMerchants = () => createSelector(
  selectMerchantsListState(),
  selectMerchantsState(),
  selectBidsState(),
  (list, merchants, bids) => (
    list.map((item) => {
      const merchant = merchants[item];

      return {
        ...merchant,
        bids: take(merchant.bids, MIN_BIDS_IN_CARD_COUNT).map((bid) => bids[bid]),
        showMoreBids: merchant.bids.length > MIN_BIDS_IN_CARD_COUNT,
      };
    })
  ),
);

export const selectModal = () => createSelector(
  selectMerchantModalState(),
  selectActiveMerchantIdState(),
  (isModalOpen, activeMerchantId) => ({
    isOpen: isModalOpen,
    title: activeMerchantId ? 'Edit Merchant' : 'Add Merchant',
  })
);

export const selectBidsModal = () => createSelector(
  selectActiveMerchantIdState(),
  selectBidsModalState(),
  selectMerchantsState(),
  selectBidsState(),
  (activeMerchantId, isModalOpen, merchants, bids) => {
    if (isModalOpen) {
      return {
        isOpen: isModalOpen,
        bids: merchants[activeMerchantId].bids.map((bidId) => bids[bidId]),
      }
    }

    return {
      isOpen: false,
      bids: [],
    }
  }
);

export const selectPaginator = () => createSelector(
  selectPerPage(),
  selectTotalResults(),
  selectCurrentPage(),
  (perPage, totalResults, currentPage) => {
    return {
      currentPage: currentPage + 1,
      totalPages: Math.floor(totalResults / perPage) + 1,
    };
  }
);
