import rootReducer from '../reducers';

describe('Root Reducer', () => {
  it('combines and nests reducers correctly', () => {
    expect(Object.keys(rootReducer(undefined, {}))).toEqual([
      'merchantList',
    ]);
  });
});
