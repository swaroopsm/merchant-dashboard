import React, { Component } from 'react';
import logo from './logo.png';
import './App.css';
import MerchantList from 'containers/MerchantList';
import 'styles/global';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </div>
        <div className="App-container">
          <MerchantList />
        </div>
      </div>
    );
  }
}

export default App;
