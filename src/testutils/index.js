import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'

// Injecting jest as a dependency injection for now
export function mockApiRequests(args = [], jest) {
  return args.map((arg) => ({
    [arg]: jest.fn().mockImplementation((data) => (
      new Promise((resolve, reject) => {
        resolve(data);
      })
    ))
  }))
}

export const configureMockStore = configureStore([
  thunk,
]);
